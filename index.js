let history = []
let finish = false
const result = document.querySelector('.result')
const resultText = document.querySelector('.result p')
const firstChild = result.children[0]
const secondChild = result.children[1]
const refresh = document.querySelector('.refresh img')

refresh.addEventListener('click', function(e){
    if(finish == false) return
    secondChild.style.display = 'block'
    firstChild.classList.remove('d-flex')
    firstChild.classList.add('d-none')
    chooseEnemy.forEach(function(item, pos){
        item.children[0].style.opacity = '1'
    })
    choosePlayer.forEach(function(item, pos){
        item.children[0].style.opacity = '1'    
    })
    finish = false

})
const listChoose = ['Batu','Kertas', 'Gunting']
const choosePlayer = document.querySelectorAll('.player')

const chooseEnemy = document.querySelectorAll('.enemy')
choosePlayer.forEach((item, i)=>{
    console.log(i)
    item.addEventListener('click', function(e){
        if (finish) return
        finish = true
        const choose  = listChoose[i]
        // const choose = e.currentTarget.innerHTML
        const  rnd = Math.floor(Math.random() * listChoose.length)
        const enemy = listChoose[rnd]
        chooseEnemy.forEach(function(item, pos){
            if(pos != rnd){
                item.children[0].style.opacity = '0.5'
            }else{
                item.children[0].style.opacity = '1'
            }
        })
        choosePlayer.forEach(function(item, pos){
            if(pos != i){
                item.children[0].style.opacity = '0.5'
            }else{
                item.children[0].style.opacity = '1'
            }
        })
        
        secondChild.style.display = 'none'
        firstChild.classList.remove('d-none')
    firstChild.classList.add('d-flex')
        const resulttext = firstChild.children[0]
        if(enemy  == choose){
            resultText.innerHTML = "DRAW"
            firstChild.children[0].style.background = '#ded149'
            history.push('seri')

        }else if ((choose == 'Batu' && enemy == 'Gunting') || (choose == 'Kertas' && enemy=='Batu') || (choose == 'Gunting' && enemy == 'Kertas')){
            resultText.innerHTML = "PLAYER WIN"
            firstChild.children[0].style.background = '#5fdea3'
            history.push('menang')
        }else{
            firstChild.children[0].style.background = '#d64545'
            resultText.innerHTML = "COM WIN"
            history.push('kalah')
        }
        console.log(history)

    })
})